FROM python:3.8
COPY . .
RUN apt --assume-yes update && apt --assume-yes upgrade && apt install findutils && xargs -a requirements/apt.txt -r apt --assume-yes install
RUN pip install -r requirements/pip.txt
ENTRYPOINT ["/entrypoint.sh"]
RUN python update_wa_env.py
CMD ["python", "register_phone.py"]
