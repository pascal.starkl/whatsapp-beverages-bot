# WhatsApp Beverages Bot

The problem is simple:

Multiple apartments in our house would like to order beverages together.

The idea is, that you should be able to send the bot your wanted amount of drinks and it merges all orders together so i can (maybe automatically) order the requested drinks from the supplier.


The Bot runs on a raspberry pi 3.

To have a seperate number i used https://www.satellite.me/ 

Whatsapp Library https://github.com/tgalal/yowsup


NOTE: This should not be necessary since i automated it in the dockerfile:
      If there are problems with the registry you have to change the Whatsapp Version and the class-md5 in /yowsup/yowsup/env_android.py 
      To get the latest version and its class-md5: https://github.com/tgalal/yowsup/issues/2979#issuecomment-657485445


What should the bot be able to do?:
    * DB Application which gets controlled through WhatsApp
    * Should be able to Add/Remove/Show wanted Drinks
    * User should be able to get the amount he needs to transfer
    * Notify User if order is near and he did not transfer the money
    * There should be a way to create f.e. a exel file of the open orders (for manual order. Admin only)
    * Maybe, order automatically from supplier
    * Maybe, Automatic receipt of payment (Notify User if payment is received)
    
    
Current struggles:
    * Creating a mostly automatic deployment through docker/docker-compose
        The problem here is, that with yowsup we first need to run the register method, so whatsapp calls/sends a sms with the verification code.
        After that we need to be able to let the user, input the verification code.
        I didn't find a way that im happy with.
        There is no way (at least i didn't find one) to wait for a user input in the docker build process.
        So we could just define a entrypoint which checks if a env variable is set.
        But that isn't a solution im happy with.
        So i'll have to check if it is possible to wait for a user input on container start.  