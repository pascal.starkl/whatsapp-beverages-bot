#!/bin/bash

if [[ -z "$METHOD" ]]; then
  echo ">>> environment variable METHOD not set"
  exit 2
fi
if [[ -z "$PHONE_NUMBER" ]]; then
  echo ">>> environment variable PHONE_NUMBER not set"
  exit 2
fi
if [[ -z "$CONFIG_CC" ]]; then
  echo ">>> environment variable CONFIG_CC not set"
  exit 2
fi
if [[ -z "$CONFIG_MCC" ]]; then
  echo ">>> environment variable CONFIG_MCC not set"
  exit 2
fi
if [[ -z "$CONFIG_MNC" ]]; then
  echo ">>> environment variable MNC not set"
    exit 2
fi
    
exec "$@"

