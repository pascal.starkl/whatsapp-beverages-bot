import os

home_dir = os.getenv("HOME")
reg_method = os.getenv("METHOD")
re_register = os.getenv("RE_REGISTER")
phone = os.getenv("PHONE_NUMBER")
config_cc = os.getenv("CONFIG_CC")
config_mcc = os.getenv("CONFIG_MCC")
config_mnc = os.getenv("CONFIG_MNC")
log_level = os.getenv("LOGLEVEL")
code = os.getenv("VER_CODE")