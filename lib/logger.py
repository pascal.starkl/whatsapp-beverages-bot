import logging
import sys

from . import env_vars

logging.basicConfig(
    format='%(asctime)s %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %I:%M:%S',
    stream=sys.stdout,
    level=env_vars.log_level or logging.INFO
)
