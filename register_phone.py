import os
import subprocess
import time

from lib import env_vars, logger

_logger = logger.logging.getLogger(__name__)
while True:
    time.sleep(100)
_logger.info("Check if phone number already exists")
if not os.path.exists(f"{env_vars.home_dir}/./.config/yowsup/{env_vars.phone}/config.json"):
    _logger.info("Register Phone")
    cmd = [
        f"yowsup-cli registration "
        f"-r {env_vars.reg_method} "
        f"--phone {env_vars.phone}"
        f"--config-cc {env_vars.config_cc}"
        f"--config-mcc {env_vars.config_mcc}"
        f"--config-mnc {env_vars.config_mnc}"
    ]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    p.wait()
    _logger.info(p.stdout)
else:
    _logger.info("Phone already exists")
