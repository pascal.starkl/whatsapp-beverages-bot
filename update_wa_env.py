from pyaxmlparser import APK
import base64
import zipfile
import hashlib
import os
import re
import subprocess
from lib import logger

"""
Set the logging level of pyaxmlparser to ERROR or else it would
spam unnecessary warnings
"""
logger.logging.getLogger("pyaxmlparser").setLevel(logger.logging.ERROR)

_logger = logger.logging.getLogger(__name__)
apk_file = "WhatsApp.apk"

# Get the yowsup dir
cmd = ["pip3 show yowsup | grep -i ^location | sed 's/Location: //g'"]
ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
yowsup_dir = ps.stdout.read().decode().strip()
# env_android.py absolute path
env_file = f"{yowsup_dir}/yowsup/env/env_android.py"
_logger.info(env_file)

if not os.path.isfile("WhatsApp.apk"):
	raise FileNotFoundError("No WhatsApp.apk found")

if not os.path.exists(env_file):
	raise FileNotFoundError(f"{env_file} does not exist")

# Todo: instead of requiring the apk, maybe just download the newest one?

_logger.info(f"Getting Version and MD5 Classes from {apk_file}")
apk = APK(apk_file)
zipFile = zipfile.ZipFile(apk_file, 'r')
classesDexFile = zipFile.read('classes.dex')
hash = hashlib.md5()
hash.update(classesDexFile)

# get the version and md5 class of the apk
version = apk.version_name
md5 = base64.b64encode(hash.digest()).decode("utf-8")

_logger.info(f"Whatsapp Version: {version}")
_logger.info(f"MD5 Classes: {md5}")

# Todo: Check if there is a better way to edit the variables of the env_android.py.

edit_md5 = False
edit_version = False

_logger.info(f"Changing the _VERSION and _MD5_CLASSES in the {env_file}")

with open(env_file, "r") as file:
	lines = file.readlines()
	# todo: this didn't work so i hardcoded the lines. -> check why this doesn't work and try not to hardcode the lines
	# for line in lines:
	# 	if "_MD5_CLASSES = \"" in line:
	# 		line =
	# 		edit_md5 = True
	# 	if re.search("\W_VERSION = \"", line):
	# 		line =
	# 		edit_version = True
	# 	if edit_version and edit_md5:
	# 		break
	lines[19] = f"    _MD5_CLASSES = \"{md5}\"\n"
	lines[22] = f"    _VERSION = \"{version}\"\n"

with open(env_file, "w") as file:
	file.writelines(lines)